// to list locally downloaded ollama models
export const listModels = async (uri: string): Promise<string[] | undefined> => {
  try {
    const response = await fetch(uri + '/api/tags')

    if (response.body === null) {
      console.error('jens --- no response.body')
      return undefined
    }

    if (!response.ok) {
      console.error('jens --- responce not okay', response.status, response.statusText)
      return undefined
    }

    const dd: string[] = []
    const reader = response.body.pipeThrough(new TextDecoderStream()).getReader()
    while (true) {
      const { value, done } = await reader.read()

      if (done) break
      const models = JSON.parse(value).models.map((m: any) => m.name as string)
      dd.push(models)
    }
    return dd
  } catch (error) {
    if (error instanceof Error) {
      console.error('jens >>> error>', error.name, error.message, JSON.stringify(error))
    } else console.error('jens --- error', String(error))
    return undefined
  }
}
