export type MessageType = {
  userName: string
  content: string
  timeDate: Date
  isQuestion: boolean
}

export type RoleType = { name: string; id: string; model: string; context: string }

export type AlterEgoType = { name: string; id: string; context: string }
