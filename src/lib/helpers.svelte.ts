export function formatDateToReadable(date: Date): string {
  const year = date.getFullYear().toString().slice(-2)
  const month = (date.getMonth() + 1).toString().padStart(2, '0')
  const day = date.getDate().toString().padStart(2, '0')
  const hours = date.getHours().toString().padStart(2, '0')
  const minutes = date.getMinutes().toString().padStart(2, '0')
  const seconds = date.getSeconds().toString().padStart(2, '0')

  const now = new Date()

  if (year === now.getFullYear().toString().slice(-2)) {
    if (month === (now.getMonth() + 1).toString().padStart(2, '0')) {
      if (day === now.getDate().toString().padStart(2, '0')) {
        return `${hours}:${minutes}:${seconds}`
      }
    }
    return `${day}-${month} ${hours}:${minutes}:${seconds}`
  }

  return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`
}
