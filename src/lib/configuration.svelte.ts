import type { MessageType } from '$lib/types.svelte'

export const DEFAULT_HOST = 'http://localhost'
export const DEFAULT_PORT = 11434

export function storedHistory() {
  let history = $state<MessageType[]>([])
  //let host: string = DEFAULT_HOST
  let host = $state<string>(DEFAULT_HOST)
  let port: number = DEFAULT_PORT
  let ls: Storage

  return {
    get history() {
      console.debug('<<(history get)>>', history)
      return history
    },

    get host() {
      console.debug('<<host>>', host)
      return host
    },

    get port() {
      console.debug('<<port>>', port)
      return port
    },

    set host(host) {
      if (host !== undefined) ls.setItem('od_host', host)
    },

    set port(port) {
      if (port !== undefined) ls.setItem('od_port', String(port))
    },

    push(message: MessageType) {
      history.push(message)
      ls.setItem('history', JSON.stringify(history))
      console.debug('<<(history push)>>', history)
    },

    /// return true if we find stored configuration (of port only)
    init(lsx: Storage): boolean {
      ls = lsx

      // read history from local storage
      const strHistory = ls.getItem('history') ?? '[]'
      history = JSON.parse(strHistory) as MessageType[]
      history = history.map((m: MessageType) => {
        return { ...m, timeDate: new Date(m.timeDate) }
      })

      // read host from local storage or assign default host
      host = ls.getItem('od_host') ?? DEFAULT_HOST

      // read port from local storage or assign default port and return uninitialized
      // if there is no stored port
      const ls_port = ls.getItem('od_port')
      if (ls_port) {
        port = Number(ls_port)
        return true
      } else {
        port = DEFAULT_PORT
        return false
      }
    },
  }
}
