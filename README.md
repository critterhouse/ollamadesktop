# Ollama Desktop

## Revolutionize Your AI Experience with Ollama Desktop

Unlock the power of artificial intelligence right from your desktop with Ollama Desktop. Designed with simplicity and security in mind, Ollama Desktop offers an **easy-to-use AI chat client** that brings the future of technology to your fingertips. Whether you’re a professional looking to streamline your workflow or an enthusiast eager to explore AI, Ollama Desktop is your perfect companion.

### Key Features (vision/in roadmap):

1. **User-Friendly AI Chat Client:** Engage in natural, intuitive conversations with our AI chat client. Its user-friendly interface ensures that you can focus on the conversation without any distractions.
2. **Local AI Model Connections:** With Ollama, your privacy is paramount. Connect to local AI models for maximum security, keeping your interactions confidential and secure within your own device.
3. **Effortless Prompt Library Management:** Say goodbye to cluttered collections. Easily manage and tag your local library of prompts, creating an organized and efficient environment for your AI explorations.
4. **Access to Global AI Prompting Library:** Expand your horizons with Ollama’s connection to an extensive global AI prompting library. Dive into a vast pool of knowledge and creativity shared by users worldwide.

### Optimized for Privacy and Convenience

Ollama Desktop is not just another AI tool; it’s a gateway to a new era of digital communication. With its focus on privacy and ease of use, Ollama stands out as a leader in the AI chat client space. Download now and start your journey with the most secure and user-friendly AI experience available.

## Start

Ollama currently needs to be started to accept all origins, e.g.:

```
OLLAMA_ORIGINS=* ollama serve
```

## Development

Start development server

```
pnpm tauri dev
```

### Make production build

Start by building the production build locally and if it works out nicely proceed with the release to the gitlab repository.

- BUild and test, i.e.

```
pnpm format
pnpm tauri build
git commit -am"<description>"
git tag -a 0.1.n -m "<description>"
git push
git push --tags
```

- Create release in gitlab
- Upload binary to release in gitlab

## Supported targets

Currently the builds are only created and tested on/for MacOS/M3. When something valuable is up and running builds for Windows and Linux will be created.

## Technologies

As all applications this one is build on the shoulders of giants including:

- Ollama (external dependency running locally)
- Tauri (framework for multi target app development)
- SvelteKit (Static web page page generation)

## Use cases

Chat with Ollama

```mermaid
sequenceDiagram
    actor User as User
    participant ODA as Ollama Desktop
    participant O as Ollama

    User ->> O: Launches Ollama
    User->>ODA: Launches Application
    User->>ODA: Enter prompt

    ODA->>O: Send prompt and chat history

    loop Loop text
        O->>ODA: Stream Response to application
        ODA-->>User: Displays Response one token at a time
    end

```
